# Changelog

## [1.6.4] - 2020-08-18

### Added

- Added several checks to pre-update group.

### Changed

- Minor adjustments to translations.

## [1.6.3] - 2020-08-05

### Fixed

- Themes installed check failing when there are many default themes.

## [1.6.2] - 2020-08-04

### Fixed

- Fix using `php artisan passport:deploy` instead of `php artisan passport:keys` in translations.

## [1.6.1] - 2020-08-03

### Fixed

- Wrong translation namespace.
- Not passing PHP extension name to translator.

## [1.6.0] - 2020-08-03

### Added

- Translations.

## [1.5.0] - 2020-08-02

### Added

- Upgraded PHP Checkup.

## [1.4.0] - 2020-08-02

### Added

- Upgraded PHP Checkup.

## [1.3.1] - 2020-07-28

### Fixed

- Changelog

## [1.3.0] - 2020-07-28

### Added

- MySQL database connection check.
- Default site theme installed post-install check.

## [1.2.0] - 2020-07-28

### Added

- InnoDB check unit tests.
- MySQL minimum version check unit tests.
- PHPInsights
- Service provider now resolves instance of `Gerardojbaez\PhpCheckup\Contracts\Manager`

### Fixed

- Not catching connection exceptions inside InnoDB check.
- Trying to get property 'version' of non-object in MySQL minimum version check.
- Only variables should be passed by reference error in MySQL minimum version check.

## [1.1.0] - 2020-07-27

### Added

- Laravel service provider
- Laravel context

## [1.0.0] - 2020-07-27

Initial release
