FROM php:7.4-fpm-alpine

WORKDIR /home/root/code

COPY --from=composer /usr/bin/composer /usr/bin/composer