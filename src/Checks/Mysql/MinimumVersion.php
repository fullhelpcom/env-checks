<?php

declare(strict_types=1);

namespace FullHelp\EnvChecks\Checks\Mysql;

use Exception;
use Gerardojbaez\PhpCheckup\Contracts\Check;
use Illuminate\Database\ConnectionInterface;

final class MinimumVersion implements Check
{
    /**
     * The minimum target version.
     *
     * @var string
     *
     * @since 0.3.0
     */
    private $targetVersion;

    /**
     * The database connection.
     *
     * @var ConnectionInterface
     */
    private $database;

    /**
     * Create a new check instance.
     */
    public function __construct(string $version, ConnectionInterface $database)
    {
        $this->targetVersion = $version;
        $this->database = $database;
    }

    /**
     * Run check.
     */
    public function check(): bool
    {
        $version = $this->getVersion();

        if ($version) {
            return version_compare(
                $version, $this->targetVersion, '>='
            ) === true;
        }

        return false;
    }

    /**
     * Get data related to this check, which can be used to format the
     * check message.
     *
     * @return string[]
     */
    public function data(): array
    {
        $version = $this->getVersion();

        if (! $version) {
            $version = '---';
        }

        return [
            'current_version' => $version,
            'target_version' => $this->targetVersion,
        ];
    }

    /**
     * Get database version.
     */
    private function getVersion(): ?string
    {
        try {
            $result = $this->database->select('SELECT VERSION() as version');
        } catch (Exception $exception) {
            return null;
        }

        if (count($result)) {
            return array_shift($result)->version;
        }

        return null;
    }
}
