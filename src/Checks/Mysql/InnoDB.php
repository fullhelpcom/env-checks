<?php

declare(strict_types=1);

namespace FullHelp\EnvChecks\Checks\Mysql;

use Exception;
use Gerardojbaez\PhpCheckup\Contracts\Check;
use Illuminate\Database\ConnectionInterface;

final class InnoDB implements Check
{
    /**
     * The database connection.
     *
     * @var ConnectionInterface
     */
    private $database;

    /**
     * Create a new check instance.
     */
    public function __construct(ConnectionInterface $database)
    {
        $this->database = $database;
    }

    /**
     * Run check.
     */
    public function check(): bool
    {
        try {
            $engines = $this->database->select('SHOW ENGINES');
        } catch (Exception $exception) {
            return false;
        }

        if (! count($engines)) {
            return false;
        }

        foreach ($engines as $engine) {
            if (strtolower($engine->Engine) !== 'innodb') {
                continue;
            }

            return in_array(strtolower($engine->Support), ['yes', 'default']);
        }

        return false;
    }

    /**
     * Get data related to this check, which can be used to format the
     * check message.
     *
     * @return string[]
     */
    public function data(): array
    {
        return [];
    }
}
