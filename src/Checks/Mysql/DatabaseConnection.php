<?php

declare(strict_types=1);

namespace FullHelp\EnvChecks\Checks\Mysql;

use Exception;
use Gerardojbaez\PhpCheckup\Contracts\Check;
use Illuminate\Database\ConnectionInterface;

/**
 * @since 1.3.0
 */
final class DatabaseConnection implements Check
{
    /**
     * The database connection.
     *
     * @var ConnectionInterface
     */
    private $database;

    /**
     * The error message.
     *
     * @var string|null
     */
    private $error = null;

    /**
     * Create a new check instance.
     */
    public function __construct(ConnectionInterface $database)
    {
        $this->database = $database;
    }

    /**
     * Run check.
     */
    public function check(): bool
    {
        try {
            $engines = $this->database->select('SHOW ENGINES');
        } catch (Exception $exception) {
            $this->error = $exception->getMessage();
            return false;
        }

        return count($engines) > 0;
    }

    /**
     * Get data related to this check, which can be used to format the
     * check message.
     *
     * @return string[]
     */
    public function data(): array
    {
        return [
            'error' => $this->error,
        ];
    }
}
