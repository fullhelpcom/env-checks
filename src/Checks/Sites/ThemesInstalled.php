<?php

declare(strict_types=1);

namespace FullHelp\EnvChecks\Checks\Sites;

use Gerardojbaez\PhpCheckup\Contracts\Check;
use Illuminate\Database\ConnectionInterface;

/**
 * @since 1.3.0
 */
final class ThemesInstalled implements Check
{
    /**
     * The database connection.
     *
     * @var ConnectionInterface
     */
    private $database;

    /**
     * Create a new check instance.
     */
    public function __construct(ConnectionInterface $database)
    {
        $this->database = $database;
    }

    /**
     * Run check.
     */
    public function check(): bool
    {
        return $this->database->table('themes')->where([
            'is_default' => 1,
        ])->count() > 0;
    }

    /**
     * Get data related to this check, which can be used to format the
     * check message.
     *
     * @return string[]
     */
    public function data(): array
    {
        return [];
    }
}
