<?php

declare(strict_types=1);

namespace FullHelp\EnvChecks;

use Gerardojbaez\PhpCheckup\Check;
use Gerardojbaez\PhpCheckup\Manager;
use FullHelp\EnvChecks\Contracts\Context;
use FullHelp\EnvChecks\Checks\Mysql\InnoDB;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Contracts\Translation\Translator;
use Gerardojbaez\PhpCheckup\Checks\MinimumSemver;
use FullHelp\EnvChecks\Checks\Mysql\MinimumVersion;
use FullHelp\EnvChecks\Checks\Sites\ThemesInstalled;
use Gerardojbaez\PhpCheckup\Checks\Php\MinimumMemory;
use FullHelp\EnvChecks\Checks\Mysql\DatabaseConnection;
use Gerardojbaez\PhpCheckup\Checks\Filesystem\Readable;
use Gerardojbaez\PhpCheckup\Checks\Filesystem\Writable;
use Gerardojbaez\PhpCheckup\Checks\Php\ExtensionIsLoaded;
use Gerardojbaez\PhpCheckup\Contracts\Manager as ManagerContract;

/**
 * A factory that creates check manager instances.
 *
 * @since 0.1.0
 */
final class Factory
{
    /**
     * The context in which these checks are being performed.
     *
     * This is where important information about the environment is stored
     * so checks can be performed. For example, the base path which is required
     * for write permission checks.
     *
     * @var ContextInterface
     */
    private $context;

    /**
     * The database connection.
     *
     * @var ConnectionInterface
     */
    private $database;

    /**
     * The translator instance.
     *
     * @var Translator
     */
    private $translator;

    /**
     * Create a new manager factory.
     */
    public function __construct(
        Context $context,
        ConnectionInterface $database,
        Translator $translator
    ) {
        $this->context = $context;
        $this->database = $database;
        $this->translator = $translator;
    }

    /**
     * Create a new checks manager.
     *
     * @since 0.1.0
     */
    public function make(): ManagerContract
    {
        $manager = new Manager();
        $manager->add($this->minimumPhpVersion());
        $manager->add($this->minimumPhpMemory());
        $manager->add($this->databaseConnection());
        $manager->add($this->minimumMysqlVersion());
        $manager->add($this->innoDbEngineSupported());
        $manager->add($this->storageDirectoryWritable());
        $manager->add($this->envFileWritable());
        $manager->add($this->siteThemesInstalled());

        $this->passportKeysDeployed($manager);
        $this->phpRequiredExtensions($manager);

        return $manager;
    }

    /**
     * PHP's minimum version.
     *
     * @since 0.1.0
     */
    private function minimumPhpVersion(): Check
    {
        $check = new Check(
            $this->translator->get('env_checks::checks.phpversion.name', [
                'target' => 'v8.1.0'
            ]),
            new MinimumSemver('8.1.0', phpversion())
        );

        $check->critical();
        $check->group('pre-install');
        $check->group('pre-update');
        $check->group('php');

        $check->passing($this->translator->get(
            'env_checks::checks.phpversion.passing'
        ));

        $check->failing($this->translator->get(
            'env_checks::checks.phpversion.failing'
        ));

        return $check;
    }

    /**
     * MySql's minimum version.
     *
     * @since 0.1.0
     */
    private function minimumMysqlVersion(): Check
    {
        $check = new Check(
            $this->translator->get('env_checks::checks.mysqlversion.name', [
                'target' => 'v8.0.0'
            ]),
            new MinimumVersion('8.0.0', $this->database)
        );

        $check->critical();
        $check->group('pre-install');
        $check->group('pre-update');
        $check->group('database');
        $check->dependsOn('db-conn', $this->translator->get(
            'env_checks::checks.mysqlversion.depends_on.db-conn'
        ));
        $check->passing($this->translator->get(
            'env_checks::checks.mysqlversion.passing'
        ));
        $check->failing($this->translator->get(
            'env_checks::checks.mysqlversion.failing'
        ));

        return $check;
    }

    /**
     * Check whether InnoDB engine is supported.
     *
     * @since 0.1.0
     */
    private function innoDbEngineSupported(): Check
    {
        $check = new Check(
            $this->translator->get('env_checks::checks.innodb.name'),
            new InnoDB($this->database)
        );

        $check->critical();
        $check->group('pre-install');
        $check->group('pre-update');
        $check->group('database');
        $check->dependsOn('db-conn', $this->translator->get(
            'env_checks::checks.innodb.depends_on.db-conn'
        ));
        $check->passing($this->translator->get(
            'env_checks::checks.innodb.passing'
        ));
        $check->failing($this->translator->get(
            'env_checks::checks.innodb.failing'
        ));

        return $check;
    }

    /**
     * PHP's minimum memory.
     *
     * @since 0.1.0
     */
    private function minimumPhpMemory(): Check
    {
        $check = new Check(
            $this->translator->get('env_checks::checks.phpmemory.name', [
                'memory' => '65MB'
            ]),
            new MinimumMemory(1024 * 65, ini_get('memory_limit'))
        );

        $check->critical();
        $check->group('pre-install');
        $check->group('pre-update');
        $check->group('php');
        $check->passing($this->translator->get(
            'env_checks::checks.phpmemory.passing'
        ));
        $check->failing($this->translator->get(
            'env_checks::checks.phpmemory.failing'
        ));

        return $check;
    }

    /**
     * Check for all required extensions.
     *
     * @since 0.1.0
     */
    private function phpRequiredExtensions(ManagerContract $manager): void
    {
        $extensions = [
            'intl', 'mbstring', 'dom', 'pdo',
            'openssl', 'xml', 'zip', 'tokenizer',
            'ctype', 'json', 'bcmath', 'curl',
            'mailparse',
        ];

        foreach ($extensions as $extension) {
            $manager->add($this->phpRequiredExtension($extension));
        }
    }

    /**
     * Check for a required extension.
     *
     * @since 0.1.0
     */
    private function phpRequiredExtension(string $extension): Check
    {
        $check = new Check(
            $this->translator->get(
                'env_checks::checks.php_req_ext.name', compact('extension')
            ),
            new ExtensionIsLoaded($extension)
        );

        $check->critical();
        $check->group('pre-install');
        $check->group('pre-update');
        $check->group('php');
        $check->passing($this->translator->get(
            'env_checks::checks.php_req_ext.passing', compact('extension')
        ));
        $check->failing($this->translator->get(
            'env_checks::checks.php_req_ext.failing', compact('extension')
        ));

        return $check;
    }

    /**
     * Check whether storage directory is writable.
     *
     * @since 0.1.0
     */
    private function storageDirectoryWritable(): Check
    {
        $path = $this->context->basePath().DIRECTORY_SEPARATOR.'storage';

        $check = new Check($this->translator->get(
            'env_checks::checks.write_directory.name', compact('path')
        ), new Writable($path));

        $check->critical();
        $check->group('pre-install');
        $check->group('post-install');
        $check->group('pre-update');
        $check->group('post-update');
        $check->group('write-permissions');
        $check->group('filesystem');

        $check->passing($this->translator->get(
            'env_checks::checks.write_directory.passing', compact('path')
        ));

        $check->failing($this->translator->get(
            'env_checks::checks.write_directory.failing', compact('path')
        ));

        return $check;
    }

    /**
     * Check whether env file is writable.
     *
     * @since 0.1.0
     */
    private function envFileWritable(): Check
    {
        $path = $this->context->basePath().DIRECTORY_SEPARATOR.'.env';

        $check = new Check($this->translator->get(
            'env_checks::checks.write_env_file.name', compact('path')
        ), new Writable($path));

        $check->critical();
        $check->group('pre-install');
        $check->group('write-permissions');
        $check->group('filesystem');

        $check->passing($this->translator->get(
            'env_checks::checks.write_env_file.passing', compact('path')
        ));

        $check->failing($this->translator->get(
            'env_checks::checks.write_env_file.failing', compact('path')
        ));

        return $check;
    }

    /**
     * Checks for Passport OAuth Keys.
     *
     * @since 0.1.0
     */
    private function passportKeysDeployed(ManagerContract $manager): void
    {
        $storage = $this->context->basePath().DIRECTORY_SEPARATOR.'storage';
        $privatePath = $storage.DIRECTORY_SEPARATOR.'oauth-private.key';
        $publicPath = $storage.DIRECTORY_SEPARATOR.'oauth-public.key';

        $private = new Check($this->translator->get(
            'env_checks::checks.private_oauth_key_readable.name'
        ), new Readable($privatePath));

        $private->passing($this->translator->get(
            'env_checks::checks.private_oauth_key_readable.passing'
        ))->failing($this->translator->get(
            'env_checks::checks.private_oauth_key_readable.failing'
        ));

        $public = new Check($this->translator->get(
            'env_checks::checks.public_oauth_key_readable.name'
        ), new Readable($publicPath));

        $private->passing($this->translator->get(
            'env_checks::checks.public_oauth_key_readable.passing'
        ))->failing($this->translator->get(
            'env_checks::checks.public_oauth_key_readable.failing'
        ));

        foreach ([$private, $public] as $check) {
            $check->critical();
            $check->group('post-install');
            $check->group('pre-update');
            $check->group('post-update');
            $manager->add($check);
        }
    }

    /**
     * Check database connection.
     *
     * @since 1.3.0
     */
    private function databaseConnection(): Check
    {
        $check = new Check($this->translator->get(
            'env_checks::checks.database_connection.name'
        ), new DatabaseConnection($this->database));

        $check->critical();
        $check->code('db-conn');
        $check->group('pre-install');
        $check->group('post-install');
        $check->group('pre-update');
        $check->group('post-update');
        $check->passing($this->translator->get(
            'env_checks::checks.database_connection.passing'
        ));
        $check->failing($this->translator->get(
            'env_checks::checks.database_connection.failing'
        ));

        return $check;
    }

    /**
     * Check site themes were installed.
     *
     * @since 1.3.0
     */
    private function siteThemesInstalled(): Check
    {
        $check = new Check($this->translator->get(
            'env_checks::checks.site_themes_installed.name'
        ), new ThemesInstalled($this->database));

        $check->critical();
        $check->group('post-install');
        $check->group('post-update');
        $check->passing($this->translator->get(
            'env_checks::checks.site_themes_installed.passing'
        ));
        $check->failing($this->translator->get(
            'env_checks::checks.site_themes_installed.failing'
        ));

        return $check;
    }
}
