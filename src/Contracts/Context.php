<?php

declare(strict_types=1);

namespace FullHelp\EnvChecks\Contracts;

interface Context
{
    /**
     * Get the base path of the application.
     */
    public function basePath(): string;
}
