<?php

declare(strict_types=1);

namespace FullHelp\EnvChecks\Laravel;

use Illuminate\Contracts\Foundation\Application;
use FullHelp\EnvChecks\Contracts\Context as ContextContract;

/**
 * Laravel context.
 *
 * @since 1.1.0
 */
final class Context implements ContextContract
{
    /**
     * The application instance.
     *
     * @var Application
     */
    private $application;

    /**
     * Create a new context.
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the base path.
     */
    public function basePath(): string
    {
        return $this->application->basePath();
    }
}
