<?php

declare(strict_types=1);

namespace FullHelp\EnvChecks\Laravel;

use FullHelp\EnvChecks\Factory;
use Gerardojbaez\PhpCheckup\Runner;
use Gerardojbaez\PhpCheckup\Contracts\Manager;
use Illuminate\Support\ServiceProvider as Provider;
use FullHelp\EnvChecks\Contracts\Context as ContextContract;
use Gerardojbaez\PhpCheckup\Contracts\Runner as RunnerContract;

/**
 * @codeCoverageIgnore
 */
final class ServiceProvider extends Provider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(ContextContract::class, Context::class);
        $this->app->bind(RunnerContract::class, Runner::class);

        $this->app->instance(Manager::class, static function ($app) {
            return (new Factory(
                new Context($app), $app['db.connection'], $app['translator']
            ))->make();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadTranslationsFrom(
            __DIR__.'/../../resources/lang', 'env_checks'
        );
    }
}
