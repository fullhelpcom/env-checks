<?php

return [
    'phpversion' => [
        'name' => 'PHP :target o más reciente',
        'passing' => 'La versión de PHP que estás utilizando es compatible: :current_version.',
        'failing' => 'Este sistema requiere PHP :target_version o más reciente, estás utilizando actualmente la versión :current_version. Para solucionar el problema, actualice PHP a la version :target_version o una más reciente.',
    ],
    'mysqlversion' => [
        'name' => 'MySQL :target o más reciente',
        'passing' => 'La versión de MySQL que estás utilizando es compatible: :current_version.',
        'failing' => 'Este sistema requiere MySQL :target_version o más reciente, estás utilizando actualmente la versión :current_version. Para solucionar el problema, actualice MySQL a la versión :target_version o una más reciente.',
        'depends_on' => ['db-conn' => 'Verificación omitida: esta verificación requiere de una conexión válida a la base de datos...']
    ],
    'innodb' => [
        'name' => 'Motor InnoDB',
        'passing' => 'El motor InnoDB está disponible para usarse.',
        'failing' => 'El motor InnoDB no está disponible en la base de datos. Por favor habilite el motor InnoDB en la base de datos MySQL o MariaDB (según aplique).',
        'depends_on' => ['db-conn' => 'Verificación omitida: esta verificación requiere de una conexión válida a la base de datos...']
    ],
    'phpmemory' => [
        'name' => ':memory de memoria mínima',
        'passing' => 'El límite de memoria de PHP es :memory_limit, lo cual es compatible.',
        'failing' => 'El límite de memoria de PHP es muy bajo. El valor mínimo para un buen funcionamiento es de :target_memory bytes, actualmente el límite es :memory_limit. Para solucionar este problema, ajuste el archivo de configuraciones php.ini y configure el valor de "memory_limit" de modo que sea igual o mayor a :target_memory bytes.',
    ],
    'php_req_ext' => [
        'name' => 'Extensión PHP: :extension',
        'passing' => 'La extensión está instalada y habilitada.',
        'failing' => 'No se encontró la extensión PHP ":extension". Verifique que la misma está instalada y habilitada en el servidor. Para instrucciones de cómo instalar y/o habilitar una extensión PHP, revise la documentación o instrucciones provistas por su servicio de alojamiento, o las instrucciones para su servidor en particular.',
    ],
    'write_directory' => [
        'name' => 'Permiso para escribir el directorio <code>:path</code>',
        'passing' => 'Se puede escribir en el directorio.',
        'failing' => 'El directorio <code>:path</code> no puede ser escrito. Para solucionar el problema, modifique los permisos del directorio de modo que el usuario del servidor tenga acceso a escribir dentro del mismo.',
    ],
    'write_env_file' => [
        'name' => 'Permiso para escribir el archivo <code>:path</code> durante la instalación',
        'passing' => 'Es posible escribir.',
        'failing' => "No es posible escribir en el archivo <code>:path</code>. Para solucionar el problema, modifique los permisos del archivo de modo que el usuario del servidor tenga acceso de escritura.",
    ],
    'private_oauth_key_readable' => [
        'name' => 'Llave privada OAuth',
        'passing' => 'La llave privada OAuth fue instalada y está lista usar.',
        'failing' => 'La llave privada OAuth no está lista para usarse. Para solucionar el problema, ejecute el comando <code>php :base_path/artisan passport:keys</code> desde la consola de comandos del servidor.',
    ],
    'public_oauth_key_readable' => [
        'name' => 'Llave pública OAuth',
        'passing' => 'La llave pública OAuth fue instalada y está lista usar.',
        'failing' => 'La llave pública OAuth no está lista para usarse. Para solucionar el problema, ejecute el comando <code>php :base_path/artisan passport:keys</code> desde la consola de comandos del servidor.',
    ],
    'database_connection' => [
        'name' => 'Conexión a la base de datos',
        'passing' => 'La conexión fue exitosa.',
        'failing' => 'No se pudo crear una conexión a la base de datos, por favor verifique los archivos de registros para información detallada del problema. Asegúrese que el nombre de la base de datos, usuario, contraseña, host, y puerto, sean correctos. Error producido: :error',
    ],
    'site_themes_installed' => [
        'name' => 'Plantillas de los sitios de conocimiento',
        'passing' => 'Las plantillas fueron instaladas con éxito.',
        'failing' => 'Las plantillas no se instalaron. Para instalar las plantillas manualmente, ejecuta el comando <code>php :base_path/artisan insert:themes</code> desde la consola de comandos del servidor.',
    ],

];
