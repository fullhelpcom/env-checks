<?php

return [
    'phpversion' => [
        'name' => 'PHP :target or newer',
        'passing' => 'You are using PHP :current_version.',
        'failing' => 'This software requires PHP :target_version or newer, you are currently using PHP :current_version. Please upgrade PHP.',
    ],
    'mysqlversion' => [
        'name' => 'MySQL :target or newer',
        'passing' => 'You are using MySQL :current_version.',
        'failing' => 'This software requires MySQL :target_version or newer, you are currently using MySQL :current_version. Please upgrade MySQL.',
        'depends_on' => ['db-conn' => 'Check skipped: check requires a working database connection...']
    ],
    'innodb' => [
        'name' => 'Database InnoDB Engine',
        'passing' => 'InnoDB Engine is supported.',
        'failing' => 'Database does not have support for InnoDB engine. Please enable MySQL InnoDB Engine.',
        'depends_on' => ['db-conn' => 'Check skipped: check requires a working database connection...']
    ],
    'phpmemory' => [
        'name' => ':memory minimum of PHP Memory Limit',
        'passing' => 'PHP Memory Limit is :memory_limit.',
        'failing' => 'PHP Memory Limit is too low. The minimum value is :target_memory bytes, it is currently set to :memory_limit. Please adjust your php.ini configuration and set memory_limit to :target_memory bytes or higher.',
    ],
    'php_req_ext' => [
        'name' => 'Required PHP Extension: :extension',
        'passing' => 'Extension is loaded.',
        'failing' => 'Extension \':extension\' not found. Make sure it is installed and loaded. Check the instructions provided by your hosting service provider or the instructions available for your server.',
    ],
    'write_directory' => [
        'name' => '<code>:path</code> directory writable',
        'passing' => '<code>:path</code> is writable.',
        'failing' => '<code>:path</code> is not writable. Adjust the directory permissions so the server\'s user is able to write <code>:path</code>.',
    ],
    'write_env_file' => [
        'name' => '<code>:path</code> writable for install',
        'passing' => '<code>:path</code> is writable.',
        'failing' => '<code>:path</code> is not writable. Adjust the file permissions so the server\'s user is able to write <code>:path</code> during the installation.',
    ],
    'private_oauth_key_readable' => [
        'name' => 'Private OAuth Key',
        'passing' => 'Private key is deployed and readable.',
        'failing' => 'Private key is either missing or not readable. To fix this, run <code>php :base_path/artisan passport:keys</code> from the command line of your server.',
    ],
    'public_oauth_key_readable' => [
        'name' => 'Public OAuth Key',
        'passing' => 'Public key is deployed and readable.',
        'failing' => 'Public key is either missing or not readable. To fix this, run <code>php :base_path/artisan passport:keys</code> from the command line of your server.',
    ],
    'database_connection' => [
        'name' => 'Database connection',
        'passing' => 'Connection to database successful.',
        'failing' => 'Could not connect to the database, please check the log files for detailed information. Make sure that the database name, username, password, host, and port are correct. You may also want to make sure that the database server is running, and there is no firewall denying the connection. Error message: :error',
    ],
    'site_themes_installed' => [
        'name' => 'Built-in site themes installed',
        'passing' => 'Site themes were installed.',
        'failing' => 'Missing site themes. Please install the default site themes by running <code>php :base_path/artisan insert:themes</code> from the command line of your server.',
    ],

];
