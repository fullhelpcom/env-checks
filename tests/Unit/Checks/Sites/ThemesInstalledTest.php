<?php

use PHPUnit\Framework\TestCase;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\ConnectionInterface;
use PHPUnit\Framework\MockObject\MockObject;
use FullHelp\EnvChecks\Checks\Sites\ThemesInstalled;

class ThemesInstalledTest extends TestCase
{
    public function testCheckReturnsTrueWhenThereIsOneMatchingRecord()
    {
        // Arrange
        /** @var Builder|MockObject */
        $query = $this->createStub(Builder::class);
        $query->expects($this->once())
            ->method('where')
            ->with(['is_default' => 1])
            ->willReturn($query);
        $query->expects($this->once())
            ->method('count')
            ->willReturn(1);

        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('table')
            ->with('themes')
            ->willReturn($query);

        $check = new ThemesInstalled($connection);

        // Act
        $result = $check->check();

        // Assert
        $this->assertTrue($result);
    }

    public function testCheckReturnsFalseWhenThereAreNoMatchingRecords()
    {
        // Arrange
        /** @var Builder|MockObject */
        $query = $this->createStub(Builder::class);
        $query->expects($this->once())
            ->method('where')
            ->with(['is_default' => 1])
            ->willReturn($query);
        $query->expects($this->once())
            ->method('count')
            ->willReturn(0);

        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('table')
            ->with('themes')
            ->willReturn($query);

        $check = new ThemesInstalled($connection);

        // Act
        $result = $check->check();

        // Assert
        $this->assertFalse($result);
    }

    public function testReturnsEmptyFormattingDataArray()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);

        $check = new ThemesInstalled($connection);

        // Act
        $result = $check->data();

        // Assert
        $this->assertSame([], $result);
    }
}
