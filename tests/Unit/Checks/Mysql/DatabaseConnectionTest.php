<?php

use PHPUnit\Framework\TestCase;
use Illuminate\Database\ConnectionInterface;
use PHPUnit\Framework\MockObject\MockObject;
use FullHelp\EnvChecks\Checks\Mysql\DatabaseConnection;

class DatabaseConnectionTest extends TestCase
{
    public function testCheckReturnsTrueWhenEnginesAreReturned()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willReturn([new stdClass]);

        $check = new DatabaseConnection($connection);

        // Act
        $result = $check->check();

        // Assert
        $this->assertTrue($result);
    }

    public function testCheckReturnsFalseWhenNoEnginesAreReturned()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willReturn([]);

        $check = new DatabaseConnection($connection);

        // Act
        $result = $check->check();

        // Assert
        $this->assertFalse($result);
    }

    public function testCheckReturnsFalseWhenExceptionIsThrown()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willThrowException(new Exception());

        $check = new DatabaseConnection($connection);

        // Act
        $result = $check->check();

        // Assert
        $this->assertFalse($result);
    }

    public function testDataReturnsErrorMessageWhenExceptionIsThrown()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willThrowException(new Exception('The error message'));

        $check = new DatabaseConnection($connection);
        $check->check();

        // Act
        $result = $check->data();

        // Assert
        $this->assertSame([
            'error' => 'The error message',
        ], $result);
    }

    public function testDataReturnsEmptyErrorMessageWhenExceptionIsNotThrown()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willReturn([]);

        $check = new DatabaseConnection($connection);
        $check->check();

        // Act
        $result = $check->data();

        // Assert
        $this->assertSame([
            'error' => null,
        ], $result);
    }
}
