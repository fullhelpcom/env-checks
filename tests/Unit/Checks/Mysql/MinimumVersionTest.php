<?php

use PHPUnit\Framework\TestCase;
use Illuminate\Database\ConnectionInterface;
use PHPUnit\Framework\MockObject\MockObject;
use FullHelp\EnvChecks\Checks\Mysql\MinimumVersion;

class MinimumVersionTest extends TestCase
{
    /** @dataProvider versionDataProvider */
    public function testCheckReturnsTrueWhenVersionIsTheSameOrNewer(
        $target,
        $current,
        $expected
    ) {
        // Arrange
        $version = new stdClass;
        $version->version = $current;

        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SELECT VERSION() as version')
            ->willReturn([$version]);

        $check = new MinimumVersion($target, $connection);

        // Act
        $result = $check->check();

        // Assert
        $this->assertSame($expected, $result);
    }

    public function testReturnsFalseWhenConnectionThrowsException()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SELECT VERSION() as version')
            ->willReturn([]);

        $check = new MinimumVersion('8.1.0', $connection);

        // Act
        $result = $check->check();

        // Assert
        $this->assertFalse($result);
    }

    public function testReturnsFalseWhenNoVersionIsReturned()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SELECT VERSION() as version')
            ->willThrowException(new Exception);

        $check = new MinimumVersion('8.1.0', $connection);

        // Act
        $result = $check->check();

        // Assert
        $this->assertFalse($result);
    }

    public function testDataReturnsFormattingData()
    {
        // Arrange
        $version = new stdClass;
        $version->version = '5.7.22';

        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SELECT VERSION() as version')
            ->willReturn([$version]);

        $check = new MinimumVersion('8.1.0', $connection);

        // Act
        $result = $check->data();

        // Assert
        $this->assertSame([
            'current_version' => '5.7.22',
            'target_version' => '8.1.0',
        ], $result);
    }

    public function testDataReturnsPlaceholderAsCurrentVersionWhenVersionCannotBeRetrieved()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SELECT VERSION() as version')
            ->willThrowException(new Exception);

        $check = new MinimumVersion('8.1.0', $connection);

        // Act
        $result = $check->data();

        // Assert
        $this->assertSame([
            'current_version' => '---',
            'target_version' => '8.1.0',
        ], $result);
    }

    public function versionDataProvider()
    {
        return [
            ['5.7.0', '5.7.0', true],
            ['5.7.0', '5.7.22', true],
            ['5.7.0', '5.6.99', false],
        ];
    }
}
