<?php

use PHPUnit\Framework\TestCase;
use FullHelp\EnvChecks\Checks\Mysql\InnoDB;
use Illuminate\Database\ConnectionInterface;
use PHPUnit\Framework\MockObject\MockObject;

class InnoDBTest extends TestCase
{
    public function testCheckReturnsTrueWhenEngineIsSupported()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willReturn([
                $this->memoryEngine(),
                $this->innoDbEngine(),
            ]);

        $check = new InnoDB($connection);

        // Act
        $result = $check->check();
        
        // Assert
        $this->assertTrue($result);
    }

    public function testReturnFalseWhenNoEnginesAreReturned()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willReturn([]);

        $check = new InnoDB($connection);

        // Act
        $result = $check->check();
        
        // Assert
        $this->assertFalse($result);
    }

    public function testReturnFalseWhenInnoDBEngineIsNotPresent()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willReturn([
                $this->memoryEngine()
            ]);

        $check = new InnoDB($connection);

        // Act
        $result = $check->check();
        
        // Assert
        $this->assertFalse($result);
    }
    
    public function testReturnFalseWhenConnectionThrowsException()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willThrowException(new Exception);

        $check = new InnoDB($connection);

        // Act
        $result = $check->check();
        
        // Assert
        $this->assertFalse($result);
    }

    public function testReturnFalseWhenInnoDBEngineIsPresentButNotSupported()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);
        $connection->expects($this->once())
            ->method('select')
            ->with('SHOW ENGINES')
            ->willReturn([
                $this->innoDbEngine('NO'),
                $this->memoryEngine(),
            ]);

        $check = new InnoDB($connection);

        // Act
        $result = $check->check();
        
        // Assert
        $this->assertFalse($result);
    }

    public function testDataReturnsEmptyArray()
    {
        // Arrange
        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createMock(ConnectionInterface::class);

        $check = new InnoDB($connection);

        // Act
        $result = $check->data();
        
        // Assert
        $this->assertSame([], $result);
    }

    private function memoryEngine()
    {
        $engine = new stdClass();
        $engine->Engine = 'MEMORY';
        $engine->Support = 'YES';
        $engine->Comment = 'Hash based, stored in memory, useful for temporary tables';
        $engine->Transactions = 'NO';
        $engine->XA = 'NO';
        $engine->Savepoints = 'NO';

        return $engine;
    }

    private function innoDbEngine($support = 'DEFAULT')
    {
        $engine = new stdClass();
        $engine->Engine = 'InnoDB';
        $engine->Support = $support;
        $engine->Comment = 'Supports for transactions, row-level locking, and foreign keys';
        $engine->Transactions = 'NO';
        $engine->XA = 'NO';
        $engine->Savepoints = 'NO';

        return $engine;
    }
}