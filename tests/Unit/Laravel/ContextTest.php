<?php

use PHPUnit\Framework\TestCase;
use Illuminate\Foundation\Application;
use FullHelp\EnvChecks\Laravel\Context;
use PHPUnit\Framework\MockObject\MockObject;

class ContextTest extends TestCase
{
    public function testBasePath()
    {
        // Arrange
        /** @var Application|MockObject $app */
        $app = $this->createMock(Application::class);
        $app->expects($this->once())
            ->method('basePath')
            ->willReturn('/var/www/some/path');

        $context = new Context($app);

        // Act
        $actual = $context->basePath();

        // Assert
        $this->assertSame('/var/www/some/path', $actual);
    }
}