<?php

use FullHelp\EnvChecks\Factory;
use PHPUnit\Framework\TestCase;
use FullHelp\EnvChecks\Contracts\Context;
use Illuminate\Database\ConnectionInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Gerardojbaez\PhpCheckup\Contracts\Manager;
use Illuminate\Contracts\Translation\Translator;

class FactoryTest extends TestCase
{
    public function testItMakesChecksManager()
    {
        // Arrange
        /** @var Context|MockObject $context */
        $context = $this->createStub(Context::class);

        /** @var ConnectionInterface|MockObject $connection */
        $connection = $this->createStub(ConnectionInterface::class);

        /** @var Translator|MockObject $translator */
        $translator = $this->createStub(Translator::class);
        $translator->method('get')->willReturnArgument(0);

        $factory = new Factory($context, $connection, $translator);

        // Act
        $manager = $factory->make();

        // Assert
        $this->assertInstanceOf(Manager::class, $manager);
        $this->assertCount(23, $manager->checks());
    }
}
