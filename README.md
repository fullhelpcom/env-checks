# Environment Checks

This package provides environment and application health checks; used for pre-install, post-install, post-updates, and general application checks.

These checks ensure that a server environment is ready to run Full Help's help desk application in optimal conditions.

This package is tested in PHP 8.1 — the recommended PHP version (or newer).
